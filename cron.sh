#!/bin/bash

# Thêm một cron job chạy vào 1 giờ đêm
(crontab -l ; echo "0 1 * * * /sbin/reboot") | crontab -

echo "Cron job đã được thêm vào lịch trình cron và sẽ chạy vào 1 giờ đêm."
