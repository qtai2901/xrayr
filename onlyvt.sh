#!/bin/bash


# Cho phép kết nối đã thiết lập và liên quan
sudo iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# Cho phép kết nối từ các phạm vi địa chỉ IP cụ thể
sudo iptables -A INPUT -p tcp -s 103.84.76.0/22 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 103.84.76.0/22 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 115.72.0.0/13 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 115.72.0.0/13 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 117.0.0.0/13 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 117.0.0.0/13 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 125.234.0.0/15 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 125.234.0.0/15 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 171.224.0.0/11 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 171.224.0.0/11 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 203.113.128.0/18 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 203.113.128.0/18 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 220.231.64.0/18 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 220.231.64.0/18 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 27.64.0.0/12 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 27.64.0.0/12 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 116.96.0.0/12 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 116.96.0.0/12 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 125.212.128.0/17 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 125.212.128.0/17 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 125.214.0.0/18 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 125.214.0.0/18 --dport 443 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 203.190.160.0/20 --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -s 203.190.160.0/20 --dport 443 -j ACCEPT

# Từ chối tất cả các kết nối không rơi vào các quy tắc trên
sudo iptables -A INPUT -p tcp --dport 80 -j DROP
sudo iptables -A INPUT -p tcp --dport 443 -j DROP
